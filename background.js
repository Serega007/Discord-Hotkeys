chrome.commands.onCommand.addListener(async function(command) {
    chrome.tabs.query({url: ['https://discord.com/app*', 'https://discord.com/channels*', 'https://discord.com/discovery*', 'https://discord.com/library*', 'https://discord.com/store*']}, (tabs) => {
    for (const tab of tabs) {
        if (command === "Переключение наушников") {
            chrome.tabs.sendMessage(tab.id, {command: 'headphones'})
            return
        }
        
        if (command === "Переключение микрофона") {
            chrome.tabs.sendMessage(tab.id, {command: 'microphone' })
            return
        }
        
        if (command === "Переключение мута вкладки дискорда") {
            chrome.tabs.update(tab.id, {muted: !tab.mutedInfo.muted}, function callback() {})
            return
        }
    }
    })
})
